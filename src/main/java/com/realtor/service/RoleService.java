package com.realtor.service;

import com.realtor.entity.Role;

import java.util.Collection;

public interface RoleService {

    Role findById(long id);

    Collection<Role> findAll();
}
