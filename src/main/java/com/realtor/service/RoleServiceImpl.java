package com.realtor.service;

import com.realtor.entity.Role;
import com.realtor.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Slf4j
@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public Role findById(long id) {
        return roleRepository.findById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<Role> findAll() {
        return roleRepository.findAll();
    }
}
