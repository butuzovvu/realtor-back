package com.realtor.service;

import com.realtor.entity.User;

public interface UserService {

    User findByLoginAndPassword(String login, String pass);

    User findById(long id);
}
